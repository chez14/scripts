// ==UserScript==
// @name        Tokopedia: Various Cleanups
// @namespace   https://gitlab.com/chez14/scripts
// @homepage    https://gitlab.com/chez14/scripts
// @match       https://www.tokopedia.com/*
// @run-at      document-start
// @supportURL  https://gitlab.com/chez14/scripts/-/issues
// @updateURL   https://gitlab.com/chez14/scripts/-/raw/main/tokopedia/cleanups.user.js
// @grant        GM_addStyle
// @version     1.0.0
// @author      chez14
// @description Various fixes and cleanups for Tokopedia Webpages.
// ==/UserScript==

function fixDarkmodeCss() {
  const customStyles = `
    [data-theme="dark"] .css-cvozl5 {
      background-color: var(--NN50);
    }
  `;

  GM_addStyle(customStyles);
}

function cleanHomeSlider() {
  const customStyles = `
    #zeus-root [data-testid="mainHomePage"] {
      margin-top: 48px;
    }
    #zeus-root [data-testid="divHomeSlider"] {
      display: none;
    }
  `;

  GM_addStyle(customStyles);
}

fixDarkmodeCss();
cleanHomeSlider();
