// ==UserScript==
// @name        Tokopedia: Disable Gojek Connector Modal
// @namespace   https://gitlab.com/chez14/scripts
// @homepage    https://gitlab.com/chez14/scripts
// @match       https://www.tokopedia.com/*
// @run-at      document-start
// @supportURL  https://gitlab.com/chez14/scripts/-/issues
// @updateURL   https://gitlab.com/chez14/scripts/-/raw/main/tokopedia/disable-gojek-connector-modal.user.js
// @grant       none
// @version     1.0.1
// @author      chez14
// @description Remove the Gojek Account Connect modal prompt in Tokopedia.
// ==/UserScript==

const log = (...args) => console.log('[chez14::disable-gojek-connector-modal]', ...args);

const LocalStorageName = "dataaclr";

const structure = JSON.parse(window.localStorage.getItem(LocalStorageName)) || {
  v: {
    can_show: false,
    exp: 1317675600000 // 3 Oct 2011, 13:00 JST.
  },
  e: 1317675600000 // 3 Oct 2011, 13:00 JST.
};

if (structure.v.exp >= (Date.now() + 604800e3)) {
  log(`${LocalStorageName} is already set. Stopping...`);
  return;
}

structure.v.can_show = false;

const date = new Date();
date.setFullYear(date.getFullYear() + 1);
structure.v.exp = date.getTime();
structure.e = date.getTime();

window.localStorage.setItem(LocalStorageName, JSON.stringify(structure));
log(`${LocalStorageName} has been updated.`);
