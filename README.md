# chez14's User Scripts

This repo contains `chez14`'s hand-crafted user scripts to help his listless
life.

**DISCLAIMER:** USE AT YOUR OWN RISK. I WILL NOT be held responsible for any
damage caused by these scripts. Please review the script thoroughly before
installing it on your browser. You have been duly warned.

Supported Extensions (arranged from most recommended to less recommended):
- [violentmonkey](https://github.com/violentmonkey/violentmonkey)
- [Tampermonkey](https://www.tampermonkey.net/)

To install the script, ensure that at least one of the recommended extensions is
installed on your browser!

## Scripts

- **Tokopedia: Disable Gojek Account Connect Modal** \
  Script designed to deactivate the Gojek Account Connect Modal when opening
  Tokopedia for the first time after a while.\
  [See code](./tokopedia/disable-gojek-connector-modal.user.js) |
  [Install](https://gitlab.com/chez14/scripts/-/raw/main/tokopedia/disable-gojek-connector-modal.user.js).

  <details>
  <summary>Modal Screenshot</summary>

  ![Modal](./.assets/tokopedia-connector-modal.png)

  </details>

## License
[AGPLv3](./LICENSE).
